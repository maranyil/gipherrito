import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faBars, faDog, faStarHalf, faSearch, faCoffee, faPaw, faHeart, faHistory } from "@fortawesome/free-solid-svg-icons";



library.add(faBars);
library.add(faDog);
library.add(faStarHalf);
library.add(faSearch);
library.add(faCoffee);
library.add(faPaw);
library.add(faHeart);
library.add(faHistory);


export default FontAwesomeIcon;